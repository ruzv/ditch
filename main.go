package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/pkg/errors"
	"gitlab.com/ruzv/ditch/bot"
	"gitlab.com/ruzv/ditch/logr"
	"gopkg.in/yaml.v3"
)

// Config Ditch config.
type Config struct {
	Log *logr.Config `yaml:"log"`
	Bot *bot.Config  `yaml:"bot"`
}

func main() {
	err := run()
	if err != nil {
		panic(err)
	}
}

// called by main.
func run() error {
	config, err := getConfig()
	if err != nil {
		return errors.Wrap(err, "failed to get config")
	}

	log := logr.NewLogR(os.Stdout, "ditch", config.Log)

	dBot, err := bot.New(log, config.Bot)
	if err != nil {
		return errors.Wrap(err, "failed to create ditch bot")
	}

	log.Infof("started")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM)
	<-sc

	log.Infof("stopping")

	err = dBot.Close()
	if err != nil {
		return errors.Wrap(err, "failed to close bot")
	}

	log.Infof("bye")

	return nil
}

func getConfig() (*Config, error) {
	b, err := os.ReadFile("config.yaml")
	if err != nil {
		return nil, errors.Wrap(err, "failed to read config file")
	}

	config := &Config{}

	err = yaml.Unmarshal(b, config)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal config")
	}

	return config, nil
}
