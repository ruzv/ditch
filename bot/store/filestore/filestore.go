package filestore

import (
	"encoding/json"
	"os"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/ruzv/ditch/bot/store"
	"gitlab.com/ruzv/ditch/logr"
)

var _ store.Storer = &Store{}

// Config file store config.
type Config struct {
	Path         string        `yaml:"path"`
	SyncInterval time.Duration `yaml:"syncInterval"`
}

// Store file store.
type Store struct {
	log          *logr.LogR
	path         string
	values       map[string]string
	valuesMu     sync.Mutex
	syncInterval time.Duration
	syncStop     chan struct{}
}

// New creates a new file store.
func New(
	log *logr.LogR,
	config *Config,
) (*Store, error) {
	log = log.Prefix("filestore")

	values, err := loadValues(log, config.Path)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load values")
	}

	s := &Store{
		log:          log,
		path:         config.Path,
		values:       values,
		syncInterval: config.SyncInterval,
		syncStop:     make(chan struct{}),
	}

	log.Infof("loaded file %q values:\n%s", config.Path, s.string())

	go s.sync()

	return s, nil
}

// Get gets the value for the key.
func (s *Store) Get(key string) (string, error) {
	s.valuesMu.Lock()
	defer s.valuesMu.Unlock()

	v, ok := s.values[key]
	if !ok {
		return "", errors.Wrapf(store.ErrKeyNotFound, "key - %q", key)
	}

	return v, nil
}

// Set sets the value for the key.
func (s *Store) Set(key, value string) error {
	s.valuesMu.Lock()
	defer s.valuesMu.Unlock()

	s.values[key] = value

	return nil
}

// Close closes the store. Stops sync, blocks until everything is closed.
func (s *Store) Close() error {
	s.syncStop <- struct{}{} // trigger stop
	<-s.syncStop             // wait for stop to complete

	return nil
}

func (s *Store) sync() {
	log := s.log.Prefix("sync")

	t := time.NewTicker(s.syncInterval)
	defer t.Stop()

	for {
		select {
		case <-t.C:
			s.syncValues(log)
		case <-s.syncStop:
			s.syncValues(log)

			close(s.syncStop)

			return
		}
	}
}

func (s *Store) syncValues(log *logr.LogR) {
	s.valuesMu.Lock()
	defer s.valuesMu.Unlock()

	log.Debugf("syncing to file %q values:\n%s", s.path, s.string())

	f, err := os.Create(s.path)
	if err != nil {
		log.Errorf("failed to open file for writing: %s", err.Error())

		return
	}

	defer func() {
		err := f.Close() //nolint:govet
		if err != nil {
			log.Errorf("failed to close filestore file: %s", err.Error())
		}
	}()

	e := json.NewEncoder(f)
	e.SetIndent("", "  ")

	err = e.Encode(s.values)
	if err != nil {
		log.Errorf("failed to write values to file: %s", err.Error())

		return
	}
}

// string unsafe in goroutines.
func (s *Store) string() string {
	b, err := json.MarshalIndent(s.values, "", "  ")
	if err != nil {
		s.log.Errorf("failed to marshal values to string: %s", err.Error())

		return ""
	}

	return string(b)
}

func loadValues(log *logr.LogR, path string) (map[string]string, error) {
	f, err := os.Open(path) //nolint:gosec
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return nil, errors.Wrap(err, "failed to open file")
		}

		return map[string]string{}, nil
	}

	defer func() {
		err := f.Close() //nolint:govet
		if err != nil {
			log.Errorf("failed to close filestore file: %s", err.Error())
		}
	}()

	v := map[string]string{}

	err = json.NewDecoder(f).Decode(&v)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode json")
	}

	return v, nil
}
