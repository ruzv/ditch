package store

import (
	"github.com/pkg/errors"
)

// ErrKeyNotFound is returned when the key is not found in the store.
var ErrKeyNotFound = errors.New("key not found")

// Storer describes the implementation of a key-value store.
type Storer interface {
	Get(key string) (string, error)
	Set(key, value string) error
	Close() error
}
