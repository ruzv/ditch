package types

import (
	"os"
	"regexp"

	"github.com/pkg/errors"
	"gitlab.com/ruzv/ditch/logr"
	"gopkg.in/yaml.v3"
)

var _ yaml.Unmarshaler = (*EnvVar)(nil)

var envVarRe = regexp.MustCompile(`env\((.+)\)`)

// EnvVar represents an environment variable or a plain text string.
// if value is in the format env(ENV_VAR) then the ENV_VAR environment variable
// is loaded from environment and used, otherwise the string value is used.
type EnvVar struct {
	isPlainText  bool
	value        string
	line, column int
}

// UnmarshalYAML unmarshals the env var from a yaml node.
// The node can either be a plain text string or a string sourrounded by
// env(...)
func (e *EnvVar) UnmarshalYAML(value *yaml.Node) error {
	if value.Kind != yaml.ScalarNode {
		return errors.New(
			"cannot unmarshal non-scalar node as env var",
		)
	}

	if value.Tag != "!!str" {
		return errors.New(
			"cannot unmarshal non-string node as env var",
		)
	}

	e.line = value.Line
	e.column = value.Column

	matches := envVarRe.FindStringSubmatch(value.Value)

	if len(matches) != 2 {
		e.value = value.Value
		e.isPlainText = true

		return nil
	}

	e.value = os.Getenv(matches[1])
	e.isPlainText = false

	return nil
}

// Value returns the value of the env var.
func (e *EnvVar) Value(log *logr.LogR) string {
	if log != nil && e.isPlainText {
		log.Warnf(
			"env var is set in plain text in the config at line %d, column %d",
			e.line,
			e.column,
		)
	}

	return e.value
}
