package types

import "time"

// Timing config.
type Timing struct {
	Interval time.Duration `yaml:"interval"`
	Start    time.Time     `yaml:"start"`
}

// TillFirstTick returns the duration until the first tick.
func (t *Timing) TillFirstTick(now time.Time) time.Duration {
	return t.Interval - (now.Sub(t.Start) % t.Interval)
}

// Do runs the given function at the specified interval and time of timing.
func (t *Timing) Do(f func() error, stop <-chan struct{}) error {
	select {
	case <-time.After(t.TillFirstTick(time.Now())):
		err := f()
		if err != nil {
			return err
		}
	case <-stop:
		return nil
	}

	ticker := time.NewTicker(t.Interval)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			err := f()
			if err != nil {
				return err
			}
		case <-stop:
			return nil
		}
	}
}
