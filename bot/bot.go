package bot

import (
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/ruzv/ditch/bot/chat"
	"gitlab.com/ruzv/ditch/bot/guild"
	"gitlab.com/ruzv/ditch/bot/store"
	"gitlab.com/ruzv/ditch/bot/store/filestore"
	"gitlab.com/ruzv/ditch/bot/types"
	"gitlab.com/ruzv/ditch/logr"
)

// Config bot config.
type Config struct {
	DiscordToken *types.EnvVar         `yaml:"discordToken"`
	ChatEngine   chat.ChatEngineConfig `yaml:"chatEngine"`
	Store        *filestore.Config     `yaml:"store"`
	Guilds       []*guild.Config       `yaml:"guilds"`
}

// Bot describes the bot.
type Bot struct {
	log   *logr.LogR
	sess  *discordgo.Session
	store store.Storer

	guilds []*guild.Manager
}

// New creates a new bot.
func New(log *logr.LogR, config *Config) (*Bot, error) {
	log = log.Prefix("bot")

	sess, err := discordgo.New("Bot " + config.DiscordToken.Value(log))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create discord session")
	}

	sess.Identify.Intents = discordgo.IntentMessageContent | discordgo.IntentsGuildMessages //nolint:lll

	fStore, err := filestore.New(log, config.Store)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create file store")
	}

	chatEng := chat.NewChatEngine(log, &config.ChatEngine)

	guilds := make([]*guild.Manager, 0, len(config.Guilds))

	for _, guildConfig := range config.Guilds {
		guilds = append(
			guilds,
			guild.New(log, sess, chatEng, fStore, guildConfig),
		)
	}

	err = sess.Open()
	if err != nil {
		return nil, errors.Wrap(err, "failed to open session")
	}

	for _, g := range guilds {
		err := g.Start()
		if err != nil {
			return nil, errors.Wrap(err, "failed to start guild")
		}
	}

	return &Bot{
		log:    log,
		sess:   sess,
		store:  fStore,
		guilds: guilds,
	}, nil
}

// Close closes the bot.
func (b *Bot) Close() error {
	b.log.Infof("closing bot")

	for _, g := range b.guilds {
		err := g.Close()
		if err != nil {
			return errors.Wrap(err, "failed to close guild")
		}
	}

	b.log.Debugf("stopping session")

	err := b.sess.Close()
	if err != nil {
		return errors.Wrap(err, "failed to close session")
	}

	b.log.Debugf("closing store")

	err = b.store.Close()
	if err != nil {
		return errors.Wrap(err, "failed to close store")
	}

	return nil
}
