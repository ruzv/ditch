package chat

import (
	"context"
	"encoding/json"
	"sync"

	"github.com/pkg/errors"
	"github.com/sashabaranov/go-openai"
	"gitlab.com/ruzv/ditch/bot/types"
	"gitlab.com/ruzv/ditch/logr"
)

// ChatEngineConfig represents the chat engine configuration.
type ChatEngineConfig struct {
	OpenAISecretKey   *types.EnvVar `yaml:"openAISecretKey"` //nolint:tagliatelle,lll
	DefaultChatConfig *ChatConfig   `yaml:"defaultChatConfig"`
}

// ChatConfig represents the initial chat configuration.
type ChatConfig struct {
	Model             string              `yaml:"model"`
	Temperature       float32             `yaml:"temperature"`
	ResponseMaxTokens int                 `yaml:"responseMaxTokens"`
	Messages          []ChatMessageConfig `yaml:"messages"`
}

// ChatMessageConfig represents a chat message configuration.
type ChatMessageConfig struct {
	Role    string `yaml:"role"`
	Content string `yaml:"content"`
}

// ChatEngine represents a chat engine.
type ChatEngine struct {
	client *openai.Client
	log    *logr.LogR
	config *ChatConfig
}

// Chat represents a chat conversation.
type Chat struct {
	client   *openai.Client
	log      *logr.LogR
	mu       sync.Mutex
	config   *ChatConfig
	messages []openai.ChatCompletionMessage
}

// NewChatEngine creates a new chat engine with the provided configuration.
func NewChatEngine(
	log *logr.LogR, config *ChatEngineConfig,
) *ChatEngine {
	log = log.Prefix("chat")

	return &ChatEngine{
		client: openai.NewClient(config.OpenAISecretKey.Value(log)),
		log:    log,
		config: config.DefaultChatConfig,
	}
}

// NewChat creates a new chat instance with the provided configuration.
// config can be nil, then default config (provided when creating engine)
// is going to be used.
func (ce *ChatEngine) NewChat(config *ChatConfig) *Chat {
	if config == nil {
		config = ce.config
	}

	messages := make([]openai.ChatCompletionMessage, 0, len(config.Messages))
	for _, cmc := range config.Messages {
		messages = append(messages, cmc.ToOpenAIChatMessage())
	}

	return &Chat{
		client:   ce.client,
		log:      ce.log,
		config:   config,
		messages: messages,
	}
}

// WithLog replaces the inherited logger from chat engine with the specified
// one.
func (c *Chat) WithLog(log *logr.LogR) *Chat {
	c.log = log

	return c
}

// Converse sends a message to the chat engine and returns the response.
// Adding both messages (prompt and reply) to chat history.
func (c *Chat) Converse(ctx context.Context, message string) (string, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.messages = append(
		c.messages,
		openai.ChatCompletionMessage{
			Role:    openai.ChatMessageRoleUser,
			Content: message,
		},
	)

	resp, err := c.client.CreateChatCompletion(
		ctx,
		openai.ChatCompletionRequest{
			N:           1,
			Temperature: c.config.Temperature,
			Model:       c.config.Model,
			MaxTokens:   c.config.ResponseMaxTokens,
			Messages:    c.messages,
		},
	)
	if err != nil {
		return "", errors.Wrap(err, "failed to create chat completion")
	}

	c.log.Debugf(
		"chat response:\n%s",
		func() string {
			b, err := json.MarshalIndent(resp, "", "  ")
			if err != nil {
				return err.Error()
			}

			return string(b)
		}(),
	)

	if resp.Choices[0].FinishReason != "stop" {
		return "", errors.Errorf(
			"unexpected finish reason - %q, expecting reason - 'stop'",
			resp.Choices[0].FinishReason,
		)
	}

	c.messages = append(c.messages, resp.Choices[0].Message)

	return resp.Choices[0].Message.Content, nil
}

// ToOpenAIChatMessage converts ChatMessageConfig to
// openai.ChatCompletionMessage.
func (cmc *ChatMessageConfig) ToOpenAIChatMessage() openai.ChatCompletionMessage { //nolint:lll
	return openai.ChatCompletionMessage{
		Role:    cmc.Role,
		Content: cmc.Content,
	}
}
