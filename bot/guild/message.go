package guild

import (
	"context"
	"fmt"
	"math/rand/v2"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/ruzv/ditch/bot/chat"
	"gitlab.com/ruzv/ditch/logr"
)

// ErrUnknownGuild error when ditch recives evens from guilds that it does not
// know.
var ErrUnknownGuild = errors.New("unknown guild")

// ditchReplyID identifies a single conversation with ditch.
type ditchReplyID struct {
	// userID    string
	channelID string
}

// ditchReplyChat describes a conversation with ditch.
type ditchReplyChat struct {
	lastReply time.Time
	chat      *chat.Chat
}

func (m *Manager) handleMessage(
	_ *discordgo.Session,
	event *discordgo.MessageCreate,
) error {
	if event.Author.ID == m.sess.State.User.ID {
		return nil
	}

	if event.GuildID != string(m.config.ID) {
		return nil
	}

	err := m.limitMessage(event)
	if err != nil {
		return errors.Wrap(err, "failed to limit message")
	}

	err = m.ditchReply(m.log.Prefix("reply"), event)
	if err != nil {
		return errors.Wrap(err, "failed to do ditch reply")
	}

	return nil
}

func (m *Manager) limitMessage(
	event *discordgo.MessageCreate,
) error {
	for _, limiter := range m.config.MessageLimiters {
		if ChannelID(event.ChannelID) != limiter.ChannelID {
			continue
		}

		if event.Author.ID != limiter.UserID {
			continue
		}

		if allowed(limiter.AllowedProbability) {
			continue
		}

		err := m.sess.ChannelMessageDelete(event.ChannelID, event.ID)
		if err != nil {
			return errors.Wrap(err, "failed to delete message")
		}

		_, err = m.sess.ChannelMessageSend(
			event.ChannelID,
			limiter.DeniedMessage,
		)
		if err != nil {
			return errors.Wrap(err, "failed to send message")
		}

		member, err := m.sess.GuildMember(event.GuildID, event.Author.ID)
		if err != nil {
			return errors.Wrap(err, "failed to get guild member")
		}

		m.log.Infof("mention %q", member.Mention())
	}

	return nil
}

func (m *Manager) ditchReply(
	log *logr.LogR,
	event *discordgo.MessageCreate,
) error {
	for _, mention := range event.Mentions {
		if mention.ID != m.sess.State.User.ID {
			continue
		}

		if !enabledInChannel(
			ChannelID(event.ChannelID),
			m.config.DitchReplies.EnabledChannels,
		) {
			_, err := m.sess.ChannelMessageSend(
				event.ChannelID,
				"Sorry I'm not allowed to talk in this channel!",
			)
			if err != nil {
				return errors.Wrap(err, "failed to send message")
			}

			return nil
		}

		replyID := ditchReplyID{
			// userID:    m.Author.ID, // chat contexts is channel specific
			channelID: event.ChannelID,
		}

		replyChat, ok := m.ditchReplies[replyID]
		if !ok ||
			time.Now().
				After(replyChat.lastReply.Add(m.config.DitchReplies.ChatTTL)) {
			replyChat = &ditchReplyChat{
				lastReply: time.Now(),
				chat: m.chatEng.NewChat(m.config.DitchReplies.ChatConfig).
					WithLog(log.Prefix("chat")),
			}

			m.ditchReplies[replyID] = replyChat
		}

		message := fmt.Sprintf(
			"%s says: %s",
			event.Author.Username,
			event.ContentWithMentionsReplaced(),
		)

		log.Debugf("ditch mentioned in message: %q", message)

		reply, err := replyChat.chat.Converse(context.Background(), message)
		if err != nil {
			return errors.Wrap(err, "failed to converse chat")
		}

		_, err = m.sess.ChannelMessageSend(event.ChannelID, reply)
		if err != nil {
			return errors.Wrap(err, "failed to send message")
		}

		replyChat.lastReply = time.Now()
	}

	return nil
}

func enabledInChannel(
	messageChannel ChannelID,
	enabledChannels []ChannelID,
) bool {
	for _, channel := range enabledChannels {
		if channel == AllChannels {
			return true
		}

		if channel == messageChannel {
			return true
		}
	}

	return false
}

func allowed(p float64) bool {
	return p >= 1 || p > 0 && p > rand.Float64() //nolint:gosec
}
