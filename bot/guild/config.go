//revive:disable:max-public-structs
package guild

import (
	"time"

	"gitlab.com/ruzv/ditch/bot/chat"
	"gitlab.com/ruzv/ditch/bot/types"
)

// AllChannels defines all channels.
const AllChannels = ChannelID("all")

// GuildID guild id (server id).
type GuildID string

// ChannelID channel id.
type ChannelID string

// UserID user id.
type UserID string

// CounterName counter name.
type CounterName string

// Config guild config.
type Config struct {
	ID              GuildID                       `yaml:"id"`
	Reminders       []ReminderConfig              `yaml:"reminders"`
	IntervalPrompts []IntervalPromptConfig        `yaml:"intervalPrompts"`
	Counters        map[CounterName]CounterConfig `yaml:"counters"`
	MessageLimiters []MessageLimiterConfig        `yaml:"messageLimiters"`
	DitchReplies    DitchRepliesConfig            `yaml:"ditchReplies"`
}

// ReminderConfig reminder config.
type ReminderConfig struct {
	ChannelID string       `yaml:"channelID"` //nolint:tagliatelle
	Message   string       `yaml:"message"`
	Timing    types.Timing `yaml:"timeing"`
}

// IntervalPromptConfig interval prompt config.
type IntervalPromptConfig struct {
	ChannelID        string           `yaml:"channelID"` //nolint:tagliatelle
	Prompt           string           `yaml:"prompt"`
	PromptChatConfig *chat.ChatConfig `yaml:"promptChatConfig"`
	Timing           types.Timing     `yaml:"timeing"`
}

// CounterConfig counter config.
type CounterConfig struct {
	Description string `yaml:"description"`
	Message     string `yaml:"message"`
}

// MessageLimiterConfig message limiter config.
type MessageLimiterConfig struct {
	ChannelID          ChannelID `yaml:"channelID"` //nolint:tagliatelle
	UserID             string    `yaml:"userID"`    //nolint:tagliatelle
	AllowedProbability float64   `yaml:"allowedPropability"`
	DeniedMessage      string    `yaml:"deniedMessage"`
}

// DitchRepliesConfig describes how ditch bot should reply if tagged in a
// message.
type DitchRepliesConfig struct {
	EnabledChannels []ChannelID      `yaml:"enabledChannels"`
	ChatConfig      *chat.ChatConfig `yaml:"chatConfig"`
	ChatTTL         time.Duration    `yaml:"chatTtl"`
}
