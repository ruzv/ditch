package guild

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/ruzv/ditch/logr"
)

func (m *Manager) handleReady(
	_ *discordgo.Session,
	event *discordgo.Ready,
) error {
	m.log.Infof("READY event\nrunning as user %q", event.User.Username)

	return nil
}

func logHandlerErr[T any](
	log *logr.LogR,
	f func(s *discordgo.Session, e T) error,
) func(s *discordgo.Session, e T) {
	return func(s *discordgo.Session, e T) {
		err := f(s, e)
		if err != nil {
			log.Errorf("error in handler: %s", err)
		}
	}
}
