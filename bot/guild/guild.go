package guild

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/ruzv/ditch/bot/chat"
	"gitlab.com/ruzv/ditch/bot/store"
	"gitlab.com/ruzv/ditch/logr"
)

// Manager manages a single guild. With all its functionality.
type Manager struct {
	log     *logr.LogR
	config  *Config
	sess    *discordgo.Session
	chatEng *chat.ChatEngine
	store   store.Storer

	ditchReplies map[ditchReplyID]*ditchReplyChat
	cmds         map[string]*botCommand

	remindersWG   sync.WaitGroup
	stopReminders chan struct{}

	intervalPromptsWG   sync.WaitGroup
	stopIntervalPrompts chan struct{}
}

// New creates a new guild manager.
func New(
	log *logr.LogR,
	sess *discordgo.Session,
	chatEng *chat.ChatEngine,
	fStore store.Storer,
	config *Config,
) *Manager {
	log = log.Prefix(fmt.Sprintf("guild-%s", config.ID))

	m := &Manager{
		log:     log,
		config:  config,
		sess:    sess,
		chatEng: chatEng,
		store:   fStore,

		ditchReplies: make(map[ditchReplyID]*ditchReplyChat),
		cmds:         make(map[string]*botCommand),

		stopReminders:       make(chan struct{}),
		stopIntervalPrompts: make(chan struct{}),
	}

	// register handler before doing any API calls to discord.
	sess.AddHandler(logHandlerErr(log, m.handleReady))
	sess.AddHandler(logHandlerErr(log, m.handleInteraction))
	sess.AddHandler(logHandlerErr(log, m.handleMessage))

	return m
}

// Start starts the guild manager, doing API calls to discord.
func (m *Manager) Start() error {
	err := m.logGuildInfo()
	if err != nil {
		return errors.Wrap(err, "failed to log guild info")
	}

	m.startReminders()
	m.startIntervalPrompts()

	err = m.addCommands()
	if err != nil {
		m.removeCommands()

		return errors.Wrap(err, "failed to add commands")
	}

	return nil
}

// Close closes the guild manager.
func (m *Manager) Close() error {
	m.log.Debugf("closing guild manager")

	m.log.Debugf("stopping reminders")

	close(m.stopReminders)
	m.remindersWG.Wait()

	m.log.Debugf("stopping interval prompts")

	close(m.stopIntervalPrompts)
	m.intervalPromptsWG.Wait()

	m.log.Debugf("removing counters")
	m.removeCommands()

	return nil
}

func (m *Manager) startReminders() {
	for _, reminder := range m.config.Reminders {
		m.remindersWG.Add(1)

		m.log.Debugf("starting reminder for %s", reminder.ChannelID)

		go func(reminder ReminderConfig) {
			defer m.remindersWG.Done()

			err := reminder.Timing.Do(
				func() error {
					m.log.Debugf(
						"sending reminder to %s",
						reminder.ChannelID,
					)

					_, err := m.sess.ChannelMessageSend(
						reminder.ChannelID,
						reminder.Message,
					)
					if err != nil {
						return errors.Wrap(err, "failed to send message")
					}

					return nil
				},
				m.stopReminders,
			)
			if err != nil {
				m.log.Errorf("reminder timer failed: %s", err)

				return
			}

			m.log.Infof("reminder stopped for %s", reminder.ChannelID)
		}(reminder)
	}
}

func (m *Manager) startIntervalPrompts() {
	for _, iPromptConfig := range m.config.IntervalPrompts {
		m.intervalPromptsWG.Add(1)

		m.log.Debugf("starting interval prompt for %s", iPromptConfig.ChannelID)

		go func(iPromptConfig IntervalPromptConfig) {
			defer m.intervalPromptsWG.Done()

			err := iPromptConfig.Timing.Do(
				func() error {
					m.log.Debugf("%s", iPromptConfig.Prompt)

					resp, err := m.chatEng.NewChat(
						iPromptConfig.PromptChatConfig,
					).
						Converse(context.Background(), iPromptConfig.Prompt)
					if err != nil {
						return errors.Wrap(err, "failed to converse")
					}

					_, err = m.sess.ChannelMessageSend(
						iPromptConfig.ChannelID, resp,
					)
					if err != nil {
						return errors.Wrap(err, "failed to send message")
					}

					return nil
				},
				m.stopIntervalPrompts,
			)
			if err != nil {
				m.log.Errorf("interval prompt timer failed: %s", err)

				return
			}

			m.log.Infof(
				"interval prompt stopped for %s",
				iPromptConfig.ChannelID,
			)
		}(iPromptConfig)
	}
}

func (m *Manager) logGuildInfo() error {
	guild, err := m.sess.Guild(string(m.config.ID))
	if err != nil {
		return errors.Wrap(err, "failed to get guild")
	}

	guildInfo := struct {
		Name     string            `json:"name"`
		Members  map[string]string `json:"members"`  // id -> username
		Channels map[string]string `json:"channels"` // id -> name
	}{
		Name:     guild.Name,
		Members:  make(map[string]string),
		Channels: make(map[string]string),
	}

	members, err := m.sess.GuildMembers(string(m.config.ID), "", 1000)
	if err != nil {
		return errors.Wrap(err, "failed to get guild members")
	}

	for _, member := range members {
		guildInfo.Members[member.User.ID] = member.User.Username
	}

	channels, err := m.sess.GuildChannels(string(m.config.ID))
	if err != nil {
		return errors.Wrap(err, "failed to get guild channels")
	}

	for _, channel := range channels {
		guildInfo.Channels[channel.ID] = channel.Name
	}

	info, err := json.MarshalIndent(guildInfo, "", "  ")
	if err != nil {
		return errors.Wrap(err, "failed to marshal guilds")
	}

	m.log.Infof("guild:\n%s", string(info))

	return nil
}
