package guild

import (
	"bytes"
	"fmt"
	"html/template"
	"sort"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/ruzv/ditch/bot/store"
)

type botCommand struct {
	command *discordgo.ApplicationCommand
	handler func(i *discordgo.InteractionCreate) error
}

func (m *Manager) handleInteraction(
	_ *discordgo.Session,
	event *discordgo.InteractionCreate,
) error {
	if event.GuildID != string(m.config.ID) {
		return nil
	}

	name := event.ApplicationCommandData().Name

	cmd, ok := m.cmds[event.ApplicationCommandData().Name]
	if !ok {
		return errors.Wrapf(
			errors.New("unknown command"),
			"command %q not found",
			name,
		)
	}

	err := cmd.handler(event)
	if err != nil {
		return errors.Wrap(err, "failed to handle command")
	}

	return nil
}

func (m *Manager) handleCounterCommand(
	event *discordgo.InteractionCreate,
) error {
	name := event.ApplicationCommandData().Options[0].StringValue()
	key := "counter-" + name
	config := m.config.Counters[CounterName(name)]

	value, err := m.store.Get(key)
	if err != nil {
		if !errors.Is(err, store.ErrKeyNotFound) {
			return errors.Wrap(err, "failed to get counter value")
		}

		value = "0"
	}

	valueInt, err := strconv.Atoi(value)
	if err != nil {
		return errors.Wrapf(
			err,
			"failed to convert counter %q value %q to int",
			name,
			value,
		)
	}

	valueInt++

	err = m.store.Set(key, strconv.Itoa(valueInt))
	if err != nil {
		return errors.Wrap(err, "failed to set counter value")
	}

	tmpl, err := template.New("").Parse(config.Message)
	if err != nil {
		return errors.Wrap(err, "failed to parse template")
	}

	w := &bytes.Buffer{}

	err = tmpl.Execute(
		w,
		struct {
			Name        string
			Description string
			Value       int
		}{
			Name:        name,
			Description: config.Description,
			Value:       valueInt,
		},
	)
	if err != nil {
		return errors.Wrap(err, "failed to execute template")
	}

	err = m.sess.InteractionRespond(
		event.Interaction,
		&discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: w.String(),
			},
		},
	)
	if err != nil {
		return errors.Wrap(err, "failed to respond to interaction")
	}

	return nil
}

func (m *Manager) handleCountersCommand(
	event *discordgo.InteractionCreate,
) error {
	type counterMessageEntry struct {
		counter string
		value   int
	}

	counters := make([]counterMessageEntry, 0, len(m.config.Counters))

	for name, config := range m.config.Counters {
		rawValue, err := m.store.Get("counter-" + string(name))
		if err != nil {
			if !errors.Is(err, store.ErrKeyNotFound) {
				return errors.Wrap(err, "failed to get counter value")
			}

			rawValue = "0"
		}

		value, err := strconv.Atoi(rawValue)
		if err != nil {
			return errors.Wrap(err, "failed to convert counter value to int")
		}

		counters = append(
			counters,
			counterMessageEntry{
				counter: config.Description,
				value:   value,
			},
		)
	}

	sort.Slice(
		counters,
		func(i, j int) bool {
			return counters[i].value > counters[j].value
		},
	)

	var message string

	for _, counter := range counters {
		message += fmt.Sprintf("- %s: %d\n", counter.counter, counter.value)
	}

	err := m.sess.InteractionRespond(
		event.Interaction,
		&discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "# Counter stats\n" + message,
			},
		},
	)
	if err != nil {
		return errors.Wrap(err, "failed to respond to interaction")
	}

	return nil
}

func (m *Manager) addCommands() error {
	if len(m.config.Counters) == 0 {
		return nil
	}

	choices := make(
		[]*discordgo.ApplicationCommandOptionChoice,
		0,
		len(m.config.Counters),
	)

	for counterName, counterConfig := range m.config.Counters {
		choices = append(
			choices,
			&discordgo.ApplicationCommandOptionChoice{
				Name:  counterConfig.Description,
				Value: counterName,
			},
		)
	}

	m.log.Infof("adding counter command")

	counterCommand, err := m.sess.ApplicationCommandCreate(
		m.sess.State.User.ID,
		string(m.config.ID),
		&discordgo.ApplicationCommand{
			Name:        "counter",
			Description: "Increase a counter",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:        "counter-name",
					Description: "Counter name",
					Type:        discordgo.ApplicationCommandOptionString,
					Choices:     choices,
					Required:    true,
				},
			},
		},
	)
	if err != nil {
		return errors.Wrap(err, "failed to create counter command")
	}

	_, ok := m.cmds["counter"]
	if !ok {
		m.cmds["counter"] = &botCommand{
			handler: m.handleCounterCommand,
			command: counterCommand,
		}
	}

	m.log.Infof("adding counters command")

	countersCommand, err := m.sess.ApplicationCommandCreate(
		m.sess.State.User.ID,
		string(m.config.ID),
		&discordgo.ApplicationCommand{
			Name:        "counters",
			Description: "Display all counter counts",
		},
	)
	if err != nil {
		return errors.Wrap(err, "failed to create counters command")
	}

	_, ok = m.cmds["counters"]
	if !ok {
		m.cmds["counters"] = &botCommand{
			handler: m.handleCountersCommand,
			command: countersCommand,
		}
	}

	return nil
}

func (m *Manager) removeCommands() {
	for name, cmd := range m.cmds {
		m.log.Infof("deleting command %q", name)

		err := m.sess.ApplicationCommandDelete(
			m.sess.State.User.ID,
			cmd.command.GuildID,
			cmd.command.ID,
		)
		if err != nil {
			m.log.Errorf("failed to delete %q command: %s", name, err.Error())
		}
	}
}
