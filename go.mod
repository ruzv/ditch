module gitlab.com/ruzv/ditch

go 1.22

require (
	github.com/bwmarrin/discordgo v0.27.1
	github.com/fatih/color v1.16.0
	github.com/pkg/errors v0.9.1
	github.com/sashabaranov/go-openai v1.7.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/sys v0.14.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
